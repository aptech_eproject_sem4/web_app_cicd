<%@ page contentType="text/html;charset=UTF-8" language="java"
	isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!-- Content Header (Page header) -->
<section class="content-header"></section>
<section class="">
	<!-- Main row -->
	<div class="row">
		<div class="col-lg-12">
			<div class="wrap-currAttent">
				<!-- PRODUCT LIST -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">
							Các lớp đang học <span
								style="font-weight: 600; font-size: 20px; color: brown;">(3)</span>
						</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool"
								data-widget="collapse">
								<i class="fa fa-minus"></i>
							</button>
						</div>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<ul class="products-list product-list-in-box">
							<li class="item">
								<div class="product-img" style="background-color: aquamarine;">

								</div>
								<div class="product-info">
									<a href="" class="product-title">Mr.Khiem - C1808G1 <span
										class="product-description"> Môn: Java - 9:00AM to
											12:00AM - Buổi: 21 </span>
								</div>
							</li>
							<!-- /.item -->
							<li class="item">
								<div class="product-img" style="background-color: aquamarine;">

								</div>
								<div class="product-info">
									<a href="" class="product-title">Mr.Khiem - C1808G1 <span
										class="product-description"> Môn: Java - 9:00AM to
											12:00AM - Buổi: 21 </span>
								</div>
							</li>
							<!-- /.item -->
							<li class="item">
								<div class="product-img" style="background-color: aquamarine;">

								</div>
								<div class="product-info">
									<a href="" class="product-title">Mr.Khiem - C1808G1 <span
										class="product-description"> Môn: Java - 9:00AM to
											12:00AM - Buổi: 21 </span>
								</div>
							</li>
						</ul>
					</div>
					<!-- /.box-body -->
					<div class="box-footer text-center">
						<a href="" class="uppercase" style="font-weight: 600;">Xem
							lịch dạy tháng 9</a>
					</div>
				</div>
			</div>
		</div>
	</div>

</section>
<!-- Main content -->