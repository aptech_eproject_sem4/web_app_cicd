package c1808g1.aem_portal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AemPortalApplication {

	public static void main(String[] args) {
		SpringApplication.run(AemPortalApplication.class, args);
	}

}
