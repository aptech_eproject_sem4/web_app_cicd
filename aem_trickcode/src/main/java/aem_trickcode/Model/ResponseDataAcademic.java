package aem_trickcode.Model;

import lombok.Data;

@Data
public class ResponseDataAcademic {
    private String isDefaultValue;
    private String name;
    private String optionTitle;
    private String value;
    private String __type;
}
