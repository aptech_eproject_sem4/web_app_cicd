package aem_trickcode.Model.Dto;

import lombok.Data;

@Data
public class Session {
	private int id;
	private String name;
}
